const { readFileSync } = require('fs')
const { Prefijo } = JSON.parse(readFileSync('./info.json'))

module.exports = {
	NoToken: "[!] Use um token válido, crie um bot via https://t.me/BotFather para obter um token",
	NoDominio: "[!] Adicione um domínio para usar o método webhook",
	Iniciar: (a="", b="") => [`Olá **${a}**! Tudo bem? Espero que esteja bem, meu nome é ${b} e sou um simples bot para telegram Espero que goste, estarei a sua disposição :D\nPara ver meu menu digite: \n${Prefijo}menu\n`, "Script", "O Criador"],
	TipoMsj: ["Imagem", "Vídeo", "Áudio", "Adesivo", "Contato", "Localização", "Documento", "Animação"],
	Consola: ["EXECUTANDO", "MENSAGEM", "Por", "De", "Chat", "Privado:", "Data"],
	Capterr: ["Erro, tente novamente mais tarde...", "Ocorreu um erro inesperado u.u"],
	WlcGps: (a="", b="") => [`Bem-vindo ${a}! a este grande grupo ${b}`, `Tchau ${a}`],
	Awanta: (a="") => [`Processando, ${a} aguarde...`, `Pesquisando, ${a} aguarde...`],
	CbCmd: (a="", b="", c="", d="", e="", f="", g="", h="", i="", j="") => ["Esta ação não pertence a você... u.u", "[ APOIAR ]", "[ INFO-BOT ]", "[ VOLTE ]", `╭⦋ COMANDOS ⦌
┊
┆╭(Downloader)
├┴╮
│   ├ ${Prefijo}play <Procurar>
│   ├ ${Prefijo}ytbuscar <Procurar>
│   ├ ${Prefijo}ytv <Url/Título>
│   ╰ ${Prefijo}yta <Url/Título>
│
│╭(Sem prefixo)
╰┤
   ╰ djbot`, "ᴺᵃ̃ᵒ ᵖᵉᶜ̧ᵒ ᵈᶦⁿʰᵉᶦʳᵒ, ᵃᵖᵉⁿᵃˢ ᶜᵒᵐ ᵒ ˢᵉᵘ ᵃᵖᵒᶦᵒ ᵉˢᵗᵒᵘ ᶠᵉˡᶦᶻ :³", "Proprietário atual:", `</INFORMAÇÃO>

- Bot: (ativo)
- Nome da chave: @${a} ${b}
- Versão: ${c}
- Tempo ativo: ${d}
- Sucessos globais: ${e}
- API do Telegram: https://github.com/telegraf/telegraf
- Banco de dados: ${f}
- Plataforma: ${j[0]}
- Versão do SO: ${j[1]}
- SO básico: ${j[2]}
- Arquitetura: ${j[3]}
- Host: ${j[4]}
- Versão do NodeJs: ${g}
- Velocidade de processamento: ${h} s
- Velocidade de conexão: ${i} ms
- RAM: ${j[5]}

• Consumo de memória:
${j[6]}

${j[7] == "" ? "" : `• Uso total da CPU:
${j[7]}

• Núcleo(s) da CPU usados:
${j[8]}`}`],
	Ytplay: (a="", b="", c="", d="", e="", f="") => [`Por favor, use bem o comando, exemplo de uso:\n\n${Prefijo+a} mtc s3rl`, `🔍 Procurar por: ${a}
📌 Título: ${b}
⌚ Duração: ${c}
👀 Visualizações: ${d}
✍️ Autor: ${e}
📜 Descrição: ${f}

Enviando, aguarde...`],
	YtBsqd: (a="", b="", c="", d="", e="", f="", g="", h="", i="", j="", k="") => ["Qual é a sua pesquisa no Youtube?", `🔍 Resultados para a pesquisa de: ${a}\n\n`, `📌 Título: ${a}
🌐 ID: ${b}
⛓️ Url: ${c}
♻️ Cara: ${d}
🖼️ Imagem: ${e}
⌚ Duração: ${f}
📜 Descrição: ${g}
📆 Data de upload: ${h}
👀 Visualizações: ${i}
||
✍️ Autor: ${j}
🔗 Canal: ${k}`],
	Ytdl: (a="") => [`Por favor, use bem o comando, exemplo de uso:\n\n${Prefijo+a} https://youtu.be/ed-6VSF-GGc\n\nOu você também pode solicitar com um título\n\n${Prefijo+a} MTC S3RL`]
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
};