const { readFileSync } = require('fs')
const { Prefijo } = JSON.parse(readFileSync('./info.json'))

module.exports = {
	NoToken: "[!] Please use a valid token, create a bot via https://t.me/BotFather to get a token",
	NoDominio: "[!] Please add a domain to use the webhook method",
	Iniciar: (a="", b="") => [`Hello **${a}**! How are you? I hope you're feeling well, my name is ${b} and I'm a simple telegram bot I hope you like it, I'll be at your service :D\nTo see my menu type:\n${Prefijo}menu\n`, "Script", "Creator"],
	TipoMsj: ["Image", "Video", "Audio", "Sticker", "Contact", "Location", "Document", "Animation"],
	Consola: ["RUNNING", "MESSAGE", "By", "From", "Chat", "Private:", "Date"],
	Capterr: ["Error, try again later...", "An unexpected error occurred u.u"],
	WlcGps: (a="", b="") => [`Welcome ${a}! to this great group ${b}`, `Bye ${a}`],
	Awanta: (a="") => [`Processing, ${a} please wait...`, `Searching, ${a} please wait...`],
	CbCmd: (a="", b="", c="", d="", e="", f="", g="", h="", i="", j="") => ["This action does not belong to you... u.u", "[ SUPPORT ]", "[ INFO-BOT ]", "[ BACK ]", `╭⦋ COMMANDS ⦌
┊
┆╭(Downloader)
├┴╮
│   ├ ${Prefijo}play <Search>
│   ├ ${Prefijo}ytbuscar <Search>
│   ├ ${Prefijo}ytv <Url/Title>
│   ╰ ${Prefijo}yta <Url/Title>
│
│╭(No prefix)
╰┤
   ╰ djbot`, "ᴵ ᵈᵒⁿ'ᵗ ᵃˢᵏ ᶠᵒʳ ᵐᵒⁿᵉʸ⁻ ᵒⁿˡʸ ʷᶦᵗʰ ʸᵒᵘʳ ˢᵘᵖᵖᵒʳᵗ ᴵ ᵃᵐ ʰᵃᵖᵖʸ :³", "Current owner:", `</INFORMATION>

- Bot: (active)
- Key name: @${a} ${b}
- Version: ${c}
- Active time: ${d}
- Global hits: ${e}
- Telegram Api: https://github.com/telegraf/telegraf
- Database: ${f}
- Platform: ${j[0]}
- OS version: ${j[1]}
- OS based: ${j[2]}
- Architecture: ${j[3]}
- Host: ${j[4]}
- NodeJs version: ${g}
- Processing speed: ${h} s
- Connection speed: ${i} ms
- RAM: ${j[5]}

• Memory consumption:
${j[6]}

${j[7] == "" ? "" : `• Total CPU Usage:
${j[7]}

• CPU Core(S) Used:
${j[8]}`}`],
	Ytplay: (a="", b="", c="", d="", e="", f="") => [`Please use the command well, example of use:\n\n${Prefijo+a} mtc s3rl`, `🔍 Search for: ${a}
📌 Title: ${b}
⌚ Duration: ${c}
👀 Views: ${d}
✍️ Author: ${e}
📜 Description: ${f}

Sending, please wait...`],
	YtBsqd: (a="", b="", c="", d="", e="", f="", g="", h="", i="", j="", k="") => ["What is your search on Youtube?", `🔍 Results for the search of: ${a}\n\n`, `📌 Title: ${a}
 🌐 ID: ${b}
 ⛓️ Url: ${c}
 ♻️ Type: ${d}
 🖼️ Image: ${e}
 ⌚ Duration: ${f}
 📜 Description: ${g}
 📆 Upload date: ${h}
 👀 Views: ${i}
 ||
 ✍️ Author: ${j}
 🔗 Channel: ${k}`],
	Ytdl: (a="") => [`Please use the command well, example of use:\n\n${Prefijo+a} https://youtu.be/ed-6VSF-GGc\n\nOr you can also request with a title\n\n${Prefijo+a} MTC S3RL`]
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
};