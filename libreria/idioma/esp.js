const { readFileSync } = require('fs')
const { Prefijo } = JSON.parse(readFileSync('./info.json'))

module.exports = {
	NoToken: "[!] Por favor use un token valido, cree un bot a través de https://t.me/BotFather para obtener un token",
	NoDominio: "[!] Por favor agrege un dominio para usar el método webhook",
	Iniciar: (a="", b="") => [`Hola **${a}**! como estas?, espero que te encuentres de lo mejor, Me llamo ${b} y soy un bot simple para telegram espero te agrade, estare a tu servicio :D\nPara ver mi menu digite:\n${Prefijo}menu\n`, "Script", "Creador"],
	TipoMsj: ["Imagen", "Video", "Audio", "Sticker", "Contacto", "Ubicacion", "Documento", "Animacion"],
	Consola: ["EJECUTANDO", "MENSAJE", "Por", "De", "Chat", "Privado:", "Fecha"],
	Capterr: ["Error, vuelva a intentarlo mas tarde...", "Ocurrio un error inesperado u.u"],
	WlcGps: (a="", b="") => [`Bienbenido ${a}! a este grandioso grupo ${b}`, `Adios ${a}`],
	Awanta: (a="") => [`Procesando, ${a} por favor espere...`, `Buscando, ${a} por favor espere...`],
	CbCmd: (a="", b="", c="", d="", e="", f="", g="", h="", i="", j="") => ["Esta acción no te pertenece... u.u", "[ APOYO ]", "[ INFO-BOT ]", "[ REGRESAR ]", `╭⦋ COMANDOS ⦌
┊
┆╭(Descargador)
├┴╮
│   ├ ${Prefijo}play <Busqueda>
│   ├ ${Prefijo}ytbuscar <Busqueda>
│   ├ ${Prefijo}ytv <Url/Titulo>
│   ╰ ${Prefijo}yta <Url/Titulo>
│
│╭(Sin prefijo)
╰┤
   ╰ djbot`, "ᴺᵒ ᵖᶦᵈᵒ ᵈᶦⁿᵉʳᵒ⁻ ˢᵒˡᵒ ᶜᵒⁿ ᵗᵘ ᵃᵖᵒʸᵒ ˢᵒʸ ᶠᵉˡᶦᶻ  :³", "Dueño actual:", `</INFORMACIÓN>

- Bot: (activo)
- Nombre clave: @${a} ${b}
- Versión: ${c}
- Tiempo activo: ${d}
- Hits globales: ${e}
- Telegram Api: https://github.com/telegraf/telegraf
- Base de datos: ${f}
- Plataforma: ${j[0]}
- Versión OS: ${j[1]}
- Base OS: ${j[2]}
- Arquitectura: ${j[3]}
- Host: ${j[4]}
- Versión NodeJs: ${g}
- Velocidad de procesamiento: ${h} s
- Velocidad de conexión: ${i} ms
- RAM: ${j[5]}

• Consumo de memoria:
${j[6]}

${j[7] == "" ? "" : `• Uso total de CPU:
${j[7]}

• CPU Core(S) Usado:
${j[8]}`}`],
	Ytplay: (a="", b="", c="", d="", e="", f="") => [`Por favor use bien el comando, ejemplo de uso:\n\n${Prefijo+a} mtc s3rl`, `🔍 Busqueda para: ${a}
📌 Titulo: ${b}
⌚ Duración: ${c}
👀 Vistas: ${d}
✍️ Autor: ${e}
📜 Descripción: ${f}

Enviando, por favor espere...`],
	YtBsqd: (a="", b="", c="", d="", e="", f="", g="", h="", i="", j="", k="") => ["Cual es su búsqueda en Youtube?", `🔍 Resultados para la búsqueda de: ${a}\n\n`, `📌 Titulo: ${a}
🌐 ID: ${b}
⛓️ Url: ${c}
♻️ Tipo: ${d}
🖼️ Imagen: ${e}
⌚ Duración: ${f}
📜 Descripción: ${g}
📆 Fecha de subida: ${h}
👀 Vistas: ${i}
||
✍️ Autor: ${j}
🔗 Canal: ${k}`],
	Ytdl: (a="") => [`Por favor use bien el comando, ejemplo de uso:\n\n${Prefijo+a} https://youtu.be/ed-6VSF-GGc\n\nÓ también puede solicitar con un título\n\n${Prefijo+a} MTC S3RL`]
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
};