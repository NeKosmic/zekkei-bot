const { readFileSync } = require('fs')
const { Prefijo } = JSON.parse(readFileSync('./info.json'))

module.exports = {
	NoToken: "[!] Harap gunakan token yang valid, buat bot melalui https://t.me/BotFather untuk mendapatkan token",
	NoDominio: "[!] Harap tambahkan domain untuk menggunakan metode webhook",
	Iniciar: (a="", b="") => [`Halo **${a}**! Apa kabar? Semoga Anda baik-baik saja, nama saya ${b} dan saya adalah bot sederhana untuk telegram, semoga Anda menyukainya, saya siap melayani Anda :D\nUntuk melihat menu saya, jenis:\n${Prefijo}menu\n`, "Skrip", "Pencipta"],
	TipoMsj: ["Gambar", "Video", "Audio", "Stiker", "Kontak", "Lokasi", "Dokumen", "Animasi"],
	Consola: ["MENJALANKAN", "PESAN", "Oleh", "Dari", "Obrolan", "Pribadi:", "Tanggal"],
	Capterr: ["Kesalahan, coba lagi nanti...", "Kesalahan tak terduga terjadi u.u"],
	WlcGps: (a="", b="") => [`Selamat datang ${a}! di grup hebat ini ${b}`, `Selamat tinggal ${a}`],
	Awanta: (a="") => [`Sedang diproses, ${a} harap tunggu...`, `Mencari, ${a} harap tunggu...`],
	CbCmd: (a="", b="", c="", d="", e="", f="", g="", h="", i="", j="") => ["Tindakan ini bukan milik Anda... u.u", "[ SUPPORT ]", "[ INFO-BOT ]", "[ BACK ]", `╭⦋ PERINTAH ⦌
┊
┆╭(Pengunduh)
├┴╮
│   ├ ${Prefijo}play <Mencari>
│   ├ ${Prefijo}ytbuscar <Mencari>
│   ├ ${Prefijo}ytv <Url/Judul>
│   ╰ ${Prefijo}yta <Url/Judul>
│
│╭(Tidak ada awalan)
╰┤
   ╰ djbot`, "ˢᵃʸᵃ ᵗᶦᵈᵃᵏ ᵐᵉᵐᶦⁿᵗᵃ ᵘᵃⁿᵍ ⁻ ʰᵃⁿʸᵃ ᵈᵉⁿᵍᵃⁿ ᵈᵘᵏᵘⁿᵍᵃⁿ ᴬⁿᵈᵃ ˢᵃʸᵃ ˢᵉⁿᵃⁿᵍ :³", "Pemilik saat ini:", `</INFORMASI>

- Bot: (aktif)
- Nama kunci: @${a} ${b}
- Versi: ${c}
- Waktu aktif: ${d}
- Hit Global: ${e}
- Api Telegram: https://github.com/telegraf/telegraf
- Basis data: ${f}
- Platform: ${j[0]}
- versi OS: ${j[1]}
- OS Dasar: ${j[2]}
- Arsitektur: ${j[3]}
- Host: ${j[4]}
- Versi NodeJs: ${g}
- Kecepatan pemrosesan: ${h} s
- Kecepatan koneksi: ${i} ms
- RAM: ${j[5]}

• Konsumsi memori:
${j[6]}

${j[7] == "" ? "" : `• Penggunaan CPU Total:
${j[7]}

• Inti CPU (S) Digunakan:
${j[8]}`}`],
	Ytplay: (a="", b="", c="", d="", e="", f="") => [`Silakan gunakan perintah dengan baik, contoh penggunaan:\n\n${Prefijo+a} mtc s3rl`, `🔍 Pencarian untuk: ${a}
📌 Judul: ${b}
⌚ Durasi: ${c}
👀 Tampilan: ${d}
✍️ Pengarang: ${e}
📜 Keterangan: ${f}

Mengirim, harap tunggu...`],
	YtBsqd: (a="", b="", c="", d="", e="", f="", g="", h="", i="", j="", k="") => ["Apa pencarian Anda di Youtube?", `🔍 Hasil untuk pencarian: ${a}\n\n`, `📌 Judul: ${a}
🌐 ID: ${b}
⛓️ Url: ${c}
♻️ Pria: ${d}
🖼️ Gambar: ${e}
⌚ Durasi: ${f}
📜 Keterangan: ${g}
📆 Tanggal unggah: ${h}
👀 Tampilan: ${i}
||
✍️ Pengarang: ${j}
🔗 Saluran: ${k}`],
	Ytdl: (a="") => [`Silahkan gunakan perintah dengan baik, contoh penggunaan:\n\n${Prefijo+a} https://youtu.be/ed-6VSF-GGc\n\nAtau bisa juga request dengan judul\n\n${Prefijo+a} MTC S3RL`]
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
};